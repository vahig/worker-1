FROM ubuntu:latest
RUN apt update && apt upgrade -y && apt install wget unzip openssh-server -y
WORKDIR /home
RUN echo root:media12 | chpasswd
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
RUN /etc/init.d/ssh restart
RUN wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && unzip ngrok-stable-linux-amd64.zip
RUN ./ngrok authtoken 1ibP09H5blGqZDbU87fI5tsG6Gi_68vyefZxDMTp6BN2D3CcG && ./ngrok tcp 22
CMD (-h)
